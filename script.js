let nav = 0;
let events = [];
const calendar = document.getElementById('calendar');
const newEventModal = document.getElementById('newEventModal');
const eventText = document.getElementById('eventTextinput');
const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
const todayEvent = document.querySelector('#one')
const saveEvents = document.getElementById('two')
const monthDisplay = document.getElementById('monthDisplay')
const dataEvent = document.getElementById('eventTitleInput1')
const addEventByt = document.querySelector('#event-button')
const save = document.getElementById('saveButton')
const titleInput = document.getElementById('eventTitleInput')
const close = document.getElementById('cancelButton')
const choose = document.getElementById('event-choose')
const paragraphElement = document.createElement('p')
const titleHolder = document.createElement('p')
const divirgent = document.createElement('div')
const circle = document.createElement("p")
SavedEvents = []
const div = document.createElement('div');
const set = () => {
    div.setAttribute('id', 'hold')
    saveEvents.appendChild(div)
    saveEvents.appendChild(titleHolder)
    saveEvents.appendChild(circle)
    saveEvents.appendChild(paragraphElement)
    div.appendChild(divirgent)
    divirgent.setAttribute('class', 'aranjament')
    divirgent.appendChild(circle)
    divirgent.appendChild(titleHolder)
    divirgent.appendChild(paragraphElement)
}

function load() {
    const dt = new Date();
    if (nav !== 0) {
        dt.setMonth(new Date().getMonth() + nav);
    }
    const month = dt.getMonth();
    const year = dt.getFullYear();
    const firstDayOfMonth = new Date(year, month, 1);
    const daysInMonth = new Date(year, month + 1, 0).getDate();
    const dateString = firstDayOfMonth.toLocaleDateString('en-us', {
        weekday: 'long',
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
    });
    const paddingDays = weekdays.indexOf(dateString.split(', ')[0]);
    document.getElementById('monthDisplay').innerText =
        `${dt.toLocaleDateString('en-us', {month: 'long'})} ${year}`;
    calendar.innerHTML = '';
    for (let i = 1; i <= paddingDays + daysInMonth; i++) {
        const daySquare = document.createElement('div');
        daySquare.classList.add('day');
        if (i > paddingDays) {
            daySquare.innerText = i - paddingDays;
        } else {
            daySquare.classList.add('padding');
        }
        calendar.appendChild(daySquare);
    }
    const activate = document.querySelectorAll('.day');
    activate.forEach(value => {
        value.addEventListener("click", () => {
            activate.forEach(a => {
                a.classList.remove("active");
            });
            value.classList.add("active");
        });
    });
    document.querySelectorAll('.day').forEach(elem => {
        elem.addEventListener('click', () => {
            if (elem.classList.contains('active')) {
                todayEvent.appendChild(title)
                title.setAttribute('class', 'remove')
                const selectedDay = title.innerText = elem.textContent + " " + monthDisplay.textContent
                div.innerHTML = "";
                events.forEach(el => {
                    if (el.date === selectedDay) {
                        div.setAttribute('id', 'hold')
                        saveEvents.appendChild(divirgent)
                        set()
                        titleHolder.innerText = el.title
                        paragraphElement.innerText = el.description
                        circle.classList.add('dot')
                        circle.style.backgroundColor = el.eventColor
                    }
                })
            }
            if (elem.classList.contains('active')) {
                document.querySelector('.remove').replaceWith(title)
            }
        })
    })
}

const title = document.createElement('h1')
save.addEventListener('click', () => {
    document.createElement('div').appendChild(circle)
    set()
    if (choose.value === "important") {
        circle.classList.add('dot')
        circle.style.backgroundColor = 'red'
    } else if (choose.value === "event") {
        circle.classList.add('dot')
        circle.style.backgroundColor = 'blue'
    } else if (choose.value === "birthday") {
        circle.classList.add('dot')
        circle.style.backgroundColor = 'purple'
    } else if (choose.value === "festival") {
        circle.classList.add('dot')
        circle.style.backgroundColor = 'pink'
    }
    titleHolder.innerText = titleInput.value
    paragraphElement.innerText = eventText.value
    newEventModal.style.display = 'none'
    events.push({
        date: todayEvent.innerText,
        title: titleInput.value,
        description: eventText.value,
        eventColor: circle.style.backgroundColor
    });
    console.log(choose.value)
    console.log(events)

})

close.addEventListener('click', () => {
    newEventModal.style.display = 'none'
})
addEventByt.addEventListener('click', () => {
    newEventModal.style.display = 'block'
    dataEvent.placeholder = todayEvent.innerText
})

function initButtons() {
    document.getElementById('nextButton').addEventListener('click', () => {
        nav++;
        load();
    });
    document.getElementById('backButton').addEventListener('click', () => {
        nav--;
        load();
    });

    document.getElementById('todayButton').addEventListener('click', () => {
        nav = 0;
        load();
        const currentDay = new Date().getDate().toString();
        document.querySelectorAll('.day').forEach(elem => {
            elem.innerText === currentDay ? elem.id = 'currentDay' : false;
        })
    })
}

initButtons();
load();
